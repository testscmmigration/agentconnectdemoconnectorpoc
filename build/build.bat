setlocal
cls
set ANT_HOME=C:\WSADdev\apache-ant-1.8.2
set JAVA_HOME=C:\j2sdk1.4.2_07
set CVSROOT=:extssh:%USERNAME%@cvs.moneygram.com:/opt/cvs2
set PATH=%JAVA_HOME%\bin;%ANT_HOME%\bin;C:\Program Files (x86)\CVSNT

:LoginToCvs
cvs login

ant %1 %2 %3 %4 %5
