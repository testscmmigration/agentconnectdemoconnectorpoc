/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.moneygram.acdc.exp.AndExpression;
import com.moneygram.acdc.exp.Expression;
import com.moneygram.acdc.exp.ExpressionAggregator;
import com.moneygram.acdc.exp.Operation;
import com.moneygram.acdc.exp.OrExpression;
import com.moneygram.acdc.exp.SimpleExpression;
import com.moneygram.acdc.util.FxRates;
import com.moneygram.acdc.util.StaxUtil;
import com.moneygram.common.enum.EnumRegistry;


/**
 * @author T007
 *
 */
public class ACDCMessageConfigXMLHandler {
    
    public static final Logger log = Logger.getLogger(ACDCMessageConfigXMLHandler.class);

    public static final String MESSAGE_MAP_XML_ELEM = "MessageMaps";
    public static final String MESSAGE_KEY_XML_ELEM  = "MessageKey";
    public static final String FX_RATES_ELEM  = "FxRates";
    public static final String TEMPLATE_XML_ELEM = "Template";
    public static final String MESSAGE_XML_ELEM = "Message";
    public static final String EXP_XML_ELEM = "Exp";
    public static final String AND_EXP_XML_ELEM = "AndExp";
    public static final String OR_EXP_XML_ELEM = "OrExp";
    public static final String RESPONSE_XML_ELEM = "Response";
    public static final String FIELD_XML_ELEM = "Field";
    public static final String VALUE_XML_ELEM = "Value";
    public static final String OPER_XML_ELEM = "Oper";
    public static final String PROP_XML_ELEM = "Property";
    public static final String NAME_XML_ELEM = "Name";
    
    public static final String NAME_XML_ATTR = "name";
    public static final String EXTENDS_XML_ATTR = "extends";
    public static final String DEFAULT_XML_ATTR = "default";
    public static final String API_VERSION_ATTR = "apiVersion";
    public static final String TEMPLATE_XML_ATTR = "template";
    public static final String CASE_IN_XML_ATTR = "caseInsensitive";
    public static final String TRUE_XML_ATTR_VAL = "true";
    
    private static MessageMaps MAPS = null;

    public static synchronized final MessageMaps getMaps() throws ACDCException{
        if (MAPS==null) {
            throw new ACDCException("You must call getMaps with the appropriate" +
            		"message config file first");
        }
        
        return MAPS;
    }
    
    public static final MessageMaps getMaps(String configurationFile) throws ACDCException, IOException {
        return getMaps(false, configurationFile);
    }
    
    public synchronized static final MessageMaps getMaps(boolean reload, String configurationFile) throws ACDCException, IOException {
        if (MAPS == null || reload == true)
            reload(configurationFile);
        
        return MAPS;
    }

	private static final void reload(String configurationFile)
			throws ACDCException, IOException {
		if (configurationFile == null || configurationFile.length() == 0)
			throw new ACDCException("No valid config file specified");

		MessageMaps retMap = new MessageMaps();

		Object[] configurationFiles;

		File file = new File(configurationFile);
		if ((configurationFile.toUpperCase().endsWith(".XML")) && (file.isFile())) {
			configurationFiles = new File[] { file };

		} else if ((configurationFile.toUpperCase().endsWith(".TXT")) && (file.isFile())) {

			File f = new File(configurationFile);
			FileReader fr = new FileReader(f);
			LineNumberReader lnr = new LineNumberReader(fr);
			
			File d = f.getParentFile();
			
			List files = new ArrayList();
			while (true) {
				String line = lnr.readLine();
				if (line == null) {
					break;
				}
				
				if (line.startsWith("#")) {
					continue;
				}
				files.add(new File(d.getAbsolutePath() + "/" + line.trim()));
			}

			configurationFiles = files.toArray();
		} else {
			throw new ACDCException("No valid config file specified");
		}

		for (int i = 0; i < configurationFiles.length; i++) {

			InputStream is = null;

			try {
				
				log.info("Loading configuration file " + ((File) configurationFiles[i]).getAbsolutePath() + "...");

				is = new FileInputStream((File) configurationFiles[i]);
				if (is == null)
					throw new ACDCException("Could not find: "
							+ configurationFile);

				XMLInputFactory factory = XMLInputFactory.newInstance();
				XMLStreamReader parser = factory.createXMLStreamReader(is);

				while (true) {
					int event = parser.next();

					if (event == XMLStreamConstants.END_DOCUMENT) {
						parser.close();
						break;
					} else if (event == XMLStreamConstants.START_ELEMENT) {
						String elemName = parser.getLocalName();
						if (isTemplateElem(elemName)) {
							ArrayList al = new ArrayList(1);
							al.add(retMap);

							// Part one, read any global templates
							processTemplate(parser, retMap, al);
						} else if (isMessageElem(elemName)) {
							// Part two, read the Messages..
							processMessageElem(parser, retMap);
						} else if (isFxRateElem(elemName)) {
							// Part two, read the Messages..
							processFxElem(parser);
						} else if (isStartDocTag(elemName)) {
							// Nothing here... Just allow to pass thru
						} else
							throw new ACDCException("Unknown tag: " + elemName);

					} else if (event == XMLStreamConstants.END_ELEMENT) {
						String elemName = parser.getLocalName();
						if (!isStartDocTag(elemName)) {
							log.warn("Unkonwn end element: " + elemName);
						}
						break;
					}
				}
			} catch (XMLStreamException xmlEx) {
				throw new ACDCException("Failed to read message map file",
						xmlEx);
			} catch (FileNotFoundException e) {
				throw new ACDCException("Failed to read message map file", e);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException ex) {

					}
				}
			}
		}

		MAPS = retMap;

		log.info("Load of configuration file: " + configurationFile
				+ " completed successfully");
	}

   /**
     * @param parser
     * @param retMap
     */
    private static void processMessageElem(XMLStreamReader parser, MessageMaps retMap) 
    										throws ACDCException{
        Message retMsg = null;
        try {
            String apiName = parser.getAttributeValue(null, NAME_XML_ATTR);
            if (apiName == null) {
                throw new ACDCException("All Message entries must have a API name");
            }
            
            retMsg = new Message(apiName);

            List templateSources = new ArrayList(2);
            templateSources.add(retMap);
            templateSources.add(retMsg);
            
	        while (true) {
	            int event = parser.next();
	            
	            if (event == XMLStreamConstants.START_ELEMENT) {
	                String tagName = parser.getLocalName();
	                if (tagName.equals(TEMPLATE_XML_ELEM)) {
	                    processTemplate(parser, retMsg, templateSources);
	                }
	                else
	                if (tagName.equals(MESSAGE_KEY_XML_ELEM)) {
	                    processMessageKey(parser, retMsg);
	                }
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT) {
	                String tagName = parser.getLocalName();
	                if (isMessageElem(tagName)) {
	                    break;
	                }
	            }
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML", xmlEx);
        }
        
        retMap.addMessage(retMsg);
    }

    /**
     * @param parser
     */
    private static void processFxElem(XMLStreamReader parser) 
    										throws ACDCException{
        String value = null;
        
        try {
            //Read the fx rates
    		log.info("FX Rates (to USD)");
	        while (true) {
	            int event = parser.next();
	            if (event == XMLStreamConstants.START_ELEMENT) {
	                String tagName = parser.getLocalName();
                    value = parser.getElementText();
//		    		log.debug("Start-Curr[" + tagName + "],Rate["+value+"]");
		    		FxRates.addRxRate(tagName, value);
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT) {
	                String tagName = parser.getLocalName();
//		    		log.debug("End-tagName[" + tagName + "]");
	                if (isFxRateElem(tagName))
	                    break;
	            } else {
//		    		log.debug("processFxElem-event[" + event + "]");
	            }
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML", xmlEx);
        }
        
//        if (name == null)
//            throw new ACDCException("Could not find name of property");
//        if (value == null)
//            throw new ACDCException("Could not find value of property");
    }

    /**
     * @param parser
     * @param message
     */
    private static void processMessageKey(XMLStreamReader parser, 
            								Message message) 
    										throws ACDCException{
        boolean isDefault = false;
        String defaultStr = parser.getAttributeValue(null, DEFAULT_XML_ATTR);
        if (defaultStr != null)
            isDefault = defaultStr.equals(TRUE_XML_ATTR_VAL);
        String apiVersion = parser.getAttributeValue(null, API_VERSION_ATTR);

        MessageKey mKey = new MessageKey(isDefault, apiVersion);

        try {
	        while (true) {
	            int event = parser.next();
	            if (event == XMLStreamConstants.START_ELEMENT) {
	                String tagName = parser.getLocalName();
	                if (tagName.equals(EXP_XML_ELEM)
	                    ||
	                    tagName.equals(AND_EXP_XML_ELEM)    
	                    ||
	                    tagName.equals(OR_EXP_XML_ELEM)) {
	                    processExpression(parser, mKey);
	                }
	                else
	                if (tagName.equals(RESPONSE_XML_ELEM)) {
	                    processResponse(parser,message, mKey);
	                    
	                }
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT) {
	                String tagName = parser.getLocalName();
	                if (isMessageKeyElem(tagName))
	                    break;
	            }
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML", xmlEx);
        }
        
        
        message.addMessageKey(mKey);
    }

    /**
     * @param parser
     * @param key
     */
    private static void processResponse(XMLStreamReader parser, Message message, MessageKey key) 
    throws ACDCException{
        String templateName = parser.getAttributeValue(null, TEMPLATE_XML_ATTR);
        if (templateName == null)
            throw new ACDCException ("Template required in Response tag.");
            
        Template t = message.getTemplate(templateName);
        if (t==null)
            throw new ACDCException("Template: "+templateName+" not found in containing message");
        Response resp = new Response(t);

//		log.debug("templateName[" + templateName + "]");
        try {
            //Read the properties
	        while (true) {
	            int event = parser.next();
//	    		log.debug("event[" + event + "]");
	            if (event == XMLStreamConstants.START_ELEMENT) {
//		    		log.debug("getLocalName[" + parser.getLocalName() + "]");
	                String tagName = parser.getLocalName();
	                if (tagName.equals(PROP_XML_ELEM)) {
	                    processProperty(parser, resp);
	                }
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT) {
//		    		log.debug("getLocalName[" + parser.getLocalName() + "]");
	                String tagName = parser.getLocalName();
	                if (isResponseElem(tagName))
	                    break;
	            }
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML", xmlEx);
        }
        
        key.setResponse(resp);
    }

    /**
     * @param parser
     * @param resp
     */
    private static void processProperty(XMLStreamReader parser, Response resp) 
    					throws ACDCException{
        String name = null;
        String value = null;
        
        try {
            //Read the properties
	        while (true) {
	            int event = parser.next();
//	    		log.debug("processProperty-event[" + event + "]");
	            if (event == XMLStreamConstants.START_ELEMENT) {
	                String tagName = parser.getLocalName();
//		    		log.debug("tagName[" + tagName + "]");
	                if (tagName.equals(NAME_XML_ELEM)) {
	                    name = parser.getElementText();
	                }
	                else
	                if (tagName.equals(VALUE_XML_ELEM)) {
	                    value = parser.getElementText();
	                }
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT) {
	                String tagName = parser.getLocalName();
	                if (isPropertyElem(tagName))
	                    break;
	            }
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML", xmlEx);
        }
        
        if (name == null)
            throw new ACDCException("Could not find name of property");
        if (value == null)
            throw new ACDCException("Could not find value of property");
        
        resp.addProperty(name, value);
    }

    /**
     * @param parser
     * @param key
     */
    private static void processExpression(XMLStreamReader parser, MessageKey key) 
    										throws ACDCException{
        key.setRootExpression(processExpression(parser, (ExpressionAggregator)null));
    }

    private static Expression processExpression(XMLStreamReader parser,
            									ExpressionAggregator currentExp) 
    													throws ACDCException{
        String elemName = parser.getLocalName();
        if (elemName.equals(EXP_XML_ELEM)) {
            SimpleExpression simpExp = getSimpleExpression(parser);
            if (currentExp == null) {
                return simpExp;
            } 
            else {
                currentExp.addExpression(simpExp);
                return currentExp;
            }
        }
        
        ExpressionAggregator ea = null; 
        if (elemName.equals(AND_EXP_XML_ELEM)) {
            ea = new AndExpression();
            if (currentExp != null)
                currentExp.addExpression(ea);
        }
        else
        if (elemName.equals(OR_EXP_XML_ELEM)) {
            ea = new OrExpression();

            if (currentExp != null)
                currentExp.addExpression(ea);
        }

        try {
	        while (true) {
	            int event  = parser.next();
	            if (event == XMLStreamConstants.START_ELEMENT) {
	                processExpression(parser, ea);
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT)
	                break;
	                
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML (ExpressioN)", xmlEx);
        }
        
        return ea;
    }

    
    private static SimpleExpression getSimpleExpression (XMLStreamReader parser) 
    									throws ACDCException {
        String field = null;
        String value = null;
        Operation oper = null;
    	boolean caseInsensitive = false;
        try {
        	
        	String caseInsensitiveStr = parser.getAttributeValue(null, CASE_IN_XML_ATTR);
        	if (caseInsensitiveStr != null && caseInsensitiveStr.trim().equalsIgnoreCase("true"))
        		caseInsensitive = true;
        	
	        while (true) {
	            int event = parser.next();
	            boolean readInnerVal = false;
	            
	            if (event == XMLStreamConstants.START_ELEMENT) {
	                String tagName = parser.getLocalName();
	                if (tagName.equals(FIELD_XML_ELEM)) {
	                    field = parser.getElementText();
	                }
	                else
	                if (tagName.equals(OPER_XML_ELEM)) {
	                    oper = (Operation)EnumRegistry.getByName(Operation.class, parser.getElementText());
	                }
	                else
	                if (tagName.equals(VALUE_XML_ELEM)) {
	                    value = parser.getElementText();
	                }
	            }
	            else
	            if (event == XMLStreamConstants.END_ELEMENT) {
	                if (readInnerVal == false)
	                    break;
	                readInnerVal = false;
	            }
	        }
        }
        catch (XMLStreamException xmlEx) {
            throw new ACDCException("Failed to read XML", xmlEx);
        }
        
        return new SimpleExpression(field, value, oper, caseInsensitive);
    }
    
    /**
     * @param parser
     * @param templateToAddTo
     */
    private static void processTemplate(XMLStreamReader parser, 
            								TemplateAggregator templateToAddTo,
            								List templatesToSearchFrom) 
    										throws ACDCException{
        String templName = parser.getAttributeValue(null, NAME_XML_ATTR);
        String templExtends = parser.getAttributeValue(null, EXTENDS_XML_ATTR);
        
        if (templName == null)
            throw new ACDCException("All templates must be named");
        
        Template baseTemplate = null;
        if (templExtends != null) {
            Iterator i = templatesToSearchFrom.iterator();
            while (i.hasNext()) {
                TemplateAggregator searchAgg = (TemplateAggregator) i.next();
                baseTemplate = (Template)searchAgg.getTemplate(templExtends);
                if (baseTemplate != null)
                    break;
            }
            
            if (baseTemplate == null)
                throw new ACDCException("Could not find template named: "+templExtends);
        }
        
        
        Template retTemplate = new Template(templName, baseTemplate);

        try {
            //Advance to first tag of template data
            int event = parser.next();
            //Make sure this isn't an empty template
            if (event != XMLStreamConstants.END_ELEMENT) {
                //Read entries for template
	            StaxUtil.loadNamedTags(parser, retTemplate.getRoot());
            }
        }
        catch (XMLStreamException ex) {
            throw new ACDCException (ex);
        }
        
        //Add the new template to the list.
        templateToAddTo.addTemplate(retTemplate);
    }

    private static boolean isStartDocTag(String name) {
        return (name != null && name.equals(MESSAGE_MAP_XML_ELEM));
    }
    
    private static boolean isTemplateElem(String name) {
        return (name != null && name.equals(TEMPLATE_XML_ELEM));
    }
    
    private static boolean isMessageElem(String name) {
        return (name != null && name.equals(MESSAGE_XML_ELEM));
    }
    
    private static boolean isMessageKeyElem(String name) {
        return (name != null) && name.equals(MESSAGE_KEY_XML_ELEM);
    }

    private static boolean isResponseElem(String name) {
        return (name != null) && name.equals(RESPONSE_XML_ELEM);
    }

    private static boolean isPropertyElem(String name) {
        return (name != null) && name.equals(PROP_XML_ELEM);
    }

    private static boolean isFxRateElem(String name) {
        return (name != null) && name.equals(FX_RATES_ELEM);
    }
    
}
