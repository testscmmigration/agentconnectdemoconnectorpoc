/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc;

/**
 * @author T007
 *
 */
public class ACDCException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 
     */
    public ACDCException() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * @param message
     */
    public ACDCException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }
    /**
     * @param message
     * @param cause
     */
    public ACDCException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
    /**
     * @param cause
     */
    public ACDCException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }
}
