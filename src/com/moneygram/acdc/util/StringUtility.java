/**
 * StringUtility.java
 * 
 * Who         Date       Description
 * ----------- ---------- -----------------------------------------------
 * dandreasen  03/21/2000 Initial revision
 * dandreasen  03/29/2000 Added padString
 */
package com.moneygram.acdc.util;

import org.apache.log4j.Logger;



/**
 * StringUtility
 *
 * Some handy string handling methods.
 *
 * @author Dave Andreasen - Caribou Lake Software
 */
public class StringUtility {
	public static void writeBuffer(StringBuffer buf, String str) {
		if (str != null)
			buf.append(str);
		else
			buf.append("null");
	}

	public static void writeBuffer(StringBuffer buf, Object obj) {
		if (obj != null)
			buf.append(obj.toString());
		else
			buf.append("null");
	}

	/**
	 * Pads a String to the specified number of characters on the right
	 * Done in the message since we don't want to save the 
	 * space padded data.
	 *
	 * @param str - String to pad.  If null a string with
	 * all spaces is returned.
	 * @param length - number of characters.  Trims the string
	 * if the string is currently too long.
	 * @return the padded string.
	 */
	public static String padStringLeftJustified(
		String str,
		int length,
		char aPadChar) {
		if (str == null) {
			char[] padChars = new char[length];

			for (int i = 0; i < length; i++) {
				padChars[i] = aPadChar;
			}
			return new String(padChars);
		} else if (str.length() == length) {
			return str;
		} else if (str.length() > length) {
			return str.substring(0, length);
		} else {
			char[] strChars = str.toCharArray();
			char[] padChars = new char[length];

			System.arraycopy(strChars, 0, padChars, 0, strChars.length);
			for (int i = strChars.length; i < length; i++) {
				padChars[i] = aPadChar;
			}
			return new String(padChars);
		}
	}
	/**
	 * Pads a String to the specified number of characters on the right
	 * Done in the message since we don't want to save the 
	 * space padded data.
	 *
	 * @param str - String to pad.  If null a string with
	 * all spaces is returned.
	 * @param length - number of characters.  Trims the string
	 * if the string is currently too long.
	 * @return the padded string.
	 */
	public static String padStringRightJustified(
		String str,
		int length,
		char aPadChar) {
		if (str == null) {
			char[] padChars = new char[length];

			for (int i = length - 1; i >= 0; i--) {
				padChars[i] = aPadChar;
			}
			return new String(padChars);
		} else if (str.length() == length) {
			return str;
		} else if (str.length() > length) {
			return str.substring(0, length);
		} else {
			char[] strChars = str.toCharArray();
			char[] padChars = new char[length];

			int startPosition = length - strChars.length;

			System.arraycopy(
				strChars,
				0,
				padChars,
				startPosition,
				strChars.length);
			for (int i = 0; i < startPosition; i++) {
				padChars[i] = aPadChar;
			}
			return new String(padChars);
		}
	}

	/**
	 * Pads a String to the specified number of characters
	 * Done in the message since we don't want to save the
	 * space padded data.
	 *
	 * @param str - String to pad.  If null a string with
	 * all spaces is returned.
	 * @param length - number of characters.  Trims the string
	 * if the string is currently too long.
	 * @return the padded string.
	 */
	public static String padString(String str, int length) {

		if (str == null) {
			char[] padChars = new char[length];

			for (int i = 0; i < length; i++) {
				padChars[i] = ' ';
			}
			return new String(padChars);
		} else if (str.length() == length) {
			return str;
		} else if (str.length() > length) {
			return str.substring(0, length);
		} else {
			char[] strChars = str.toCharArray();
			char[] padChars = new char[length];

			System.arraycopy(strChars, 0, padChars, 0, strChars.length);
			for (int i = strChars.length; i < length; i++) {
				padChars[i] = ' ';
			}
			return new String(padChars);
		}
	}

	public static String getNonNullString(
		final String s,
		final String nullVersion) {
		if (s == null)
			return nullVersion;
		else
			return s;
	}

	public static String getNonNullString(final String s) {
		return getNonNullString(s, "");
	}

	public static String getNonNullZeroString(final String s) {
		return getNonNullString(s, "0");
	}

	public static boolean isNullOrEmpty(final String s) {
		return s == null || s.length() == 0;
	}


	/**
	 * Helper method for debugging.  Prints the EBCDIC bytes and the
	 * ASCII equivalent string.
	 *
	 * @param msg - A text message that indicates what the input data is
	 * @param inputData - The data in EBCDIC.  Translated to ASCII when
	 * it prints the string
	 * @param log - The log to write to.
	 */
	public static void printBytesToLogAsString(String msg, byte[] inputData, Logger log ) {
		StringBuffer buf = new StringBuffer( 1024 );
		String separator = "\r\n";
		buf.append(msg + ":");
		buf.append( " ( String: "+inputData.length+" ) : " );
		buf.append( new String( inputData ) );
		log.debug(buf.toString());
	}
	
	public static  byte[] substring(byte[] byteArrayInput, int start, int end) {
		if (byteArrayInput == null)
			return null;
		byte[] byteArrayOutput = null;
		byteArrayOutput = new byte[end - start];
		int j = 0;
		for (int i = start; i < end; i++) {
			byteArrayOutput[j] = byteArrayInput[i];
			j++;
		}
		return byteArrayOutput;
	}

	/**
	 * Given the name of the request class, return only the short class name.
	 * For example, com.moneygram.rts.message.CodeTableRequest class
	 * translates to CodeTableRequest.
	 * 
	 * @param request The request class
	 * @return The method name.
	 */
	public static String getClassNameNoPackage(String className) {	
		StringBuffer methodNameBuf = new StringBuffer(className);
					
		int index = methodNameBuf.length()-1;
		//Catch package name or last inner class
		while (index > 0 
				&& methodNameBuf.charAt(index)!='.'
				&& methodNameBuf.charAt(index)!='$'  ) {
			--index;
		}
		if (index > 0) {
			methodNameBuf.delete(0, index+1);	
		}
		
		return methodNameBuf.toString();
	}

	public static String getClassNameNoPackage(Object object) {
		if (object == null)
			return null;
			
		return getClassNameNoPackage(object.getClass().getName());
	}
	
	public static int find(StringBuffer sb, String s) {
		return find(sb, s, 0);
	}
	
	/**
	 * Given a string buffer, find the starting index of a given string
	 * @param sb The string buffer
	 * @param s The string to find
	 * @param startIndex Where to start looking
	 * @return -1 If not found, or if found a number greater than zero representing the index.
	 */
	public static int find(StringBuffer sb, String s, int startIndex) {
		if (sb == null || s == null 
			|| sb.length() == 0 || s.length()==0
			|| sb.length() < s.length())
			return -1; //Not Found
		
		int lengthOfString = s.length();
		int lengthToCheck = sb.length()-lengthOfString+1;
		
		for (int i = startIndex; i < lengthToCheck; ++i ){
			int j = 0;
			while (j < lengthOfString) {
				if (sb.charAt(i+j)!=s.charAt(j)) {
					break;
				}
				j++;
			}
			if (j == lengthOfString)
				return i;
		}
		
		return -1;
	}
				
} // class StringUtility
