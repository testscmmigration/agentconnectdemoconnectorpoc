/*
 * Created on Jun 19, 2006
 *
 */
package com.moneygram.acdc.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author T007
 *
 */
public class SOAPUtility {
	public static final String ENV_START = "<soapenv:Envelope";
    public static final String SOAP_ENV_START = "<soapenv:Envelope";
    public static final String SOAP_ENV_END = "</soapenv:Envelope";
    public static final String SOAP_BODY_START = "<soapenv:Body";
    public static final String SOAP_BODY_END= "</soapenv:Body>";
    public static final String END_TAG = ">";
    public static final String DEFAULT_NAME_SPACE = "soapenv";
    
    public static StringBuffer stipSOAP(StringBuffer request) {
    	
    	Pattern pattern = Pattern.compile("<(\\w):Envelope");
		Matcher matcher = pattern.matcher(request);
		String nameSpace=null;
		if (matcher.find()) {
			nameSpace = matcher.group(1);
		}
    	
        int soapEnvStart = StringUtility.find(request, getTagwithNameSpace(SOAP_ENV_START,nameSpace));
        int soapBodyStart = StringUtility.find(request, getTagwithNameSpace(SOAP_BODY_START,nameSpace), soapEnvStart);
        int soapBodyStartEnd = StringUtility.find(request, getTagwithNameSpace(END_TAG,nameSpace), soapBodyStart);
        
        request.delete(soapEnvStart, soapBodyStartEnd+1);
        
        int soapBodyEnd= StringUtility.find(request, getTagwithNameSpace(SOAP_BODY_END,nameSpace));
        int soapEnvEnd  = StringUtility.find(request, getTagwithNameSpace(SOAP_ENV_END,nameSpace), soapBodyEnd) + SOAP_ENV_END.length();
        request.delete(soapBodyEnd, soapEnvEnd+1);
        
        return request;
    }
    
	private static String getTagwithNameSpace(String tag, String nameSpace) {
		if (nameSpace != null) {
			return tag.replaceAll(DEFAULT_NAME_SPACE, nameSpace);
		}
		return tag;
	}
}
