/*
 * Created on Jul 11, 2006
 *
 */
package com.moneygram.acdc;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.log4j.Logger;

import com.moneygram.acdc.util.FxRates;

import flags.CompileTimeFlags;

/**
 * @author T007
 * 
 */
public class XMLElement {
	private static final Logger log = Logger.getLogger(XMLElement.class);

	public static final String SUB_PREFIX = "${";
	public static final String SUB_POSTFIX = "}";
	public static final String REQUEST_PREFIX = "Request.";
	public static final String TIMESTAMP_SUB = "timestamp";
	public static final String ADDITION_PREFIX = "ADD[";
	public static final String SUBTRACTION_PREFIX = "SUB[";
	public static final String MULTIPLICATION_PREFIX = "MULT[";
	public static final String DIVISION_PREFIX = "DIV[";
	public static final String FX_PREFIX = "FX[";
	public static final String FX_RATE_PREFIX = "FX_RATE[";
	public static final String REV_QUOTE_SEND_TOTAL_PREFIX = "REV_QUOTE_SEND_TOTAL[";
	public static final String FX_MINUS_FEE_PREFIX = "FX_MINUS_FEE[";
	public static final String RECV_AMT_PREFIX = "RECV_AMT[";
	public static final String FX_DEDUCT_FEE_PREFIX = "FX_DEDUCT_FEE[";
	public static final String MATH_POSTFIX = "]";
	public static final String REFERENCE_NUMBER = "REFERENCE_NUMBER";

	private String name;
	private String value;
	private LinkedHashMap children;
	private Map attributes;

	/**
	 * @return
	 */
	public static SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	/**
     * 
     */
	public XMLElement() {
		super();
	}

	public XMLElement(String name) {
		super();
		this.name = name;
	}

	public XMLElement(String name, String value) {
		this(name);
		this.value = value;
	}

	public void addChild(XMLElement te) {
		if (children == null) {
			children = new LinkedHashMap();
		}

		List al = (List) children.get(te.getName());
		if (al == null) {
			al = new ArrayList();
			children.put(te.getName(), al);
		}

		al.add(te);
	}
	
	public void addAttribute(String name, String value) {
		if (attributes == null) {
			attributes = new HashMap();
		}
		attributes.put(name, value);
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		final XMLElement te = new XMLElement();
		te.name = getName();
		te.value = getValue();
		if (hasChildren()) {
			te.children = new LinkedHashMap();
			final Iterator i = children.values().iterator();
			while (i.hasNext()) {
				final ArrayList childrenList = (ArrayList) i.next();
				final Iterator childListIter = childrenList.iterator();
				while (childListIter.hasNext()) {
					final XMLElement child = (XMLElement) childListIter.next();
					// Clone my child, add him to clone set.
					final XMLElement childClone = (XMLElement) child.clone();
					te.addChild(childClone);
				}
			}
		}
		if (attributes != null) {
			List attributeList = new ArrayList(attributes.keySet());
			for (int i = 0; i < attributeList.size(); i++) {
				String an = (String) attributeList.get(i);
				String av = (String) attributes.get(an);
				te.addAttribute(an, av);
			}
		}

		return te;
	}

	/**
	 * @param childrenToAdd
	 */
	public void combineChildren(XMLElement childrenToAdd) {
		if (childrenToAdd.hasChildren()) {
			final Iterator i = childrenToAdd.children.values().iterator();
			while (i.hasNext()) {
				final List childrenList = (List) i.next();
				final Iterator childrenListIter = childrenList.iterator();
				while (childrenListIter.hasNext()) {
					final XMLElement te = (XMLElement) childrenListIter.next();
					addChild(te);
				}
			}
		}
	}

	/**
	 * Returns all children in iteration order.
	 * 
	 * @return
	 */
	public Iterator getAllChildren() {
		final ArrayList al = new ArrayList();

		if (hasChildren()) {
			final Iterator i = children.values().iterator();
			while (i.hasNext()) {
				final List list = (List) i.next();
				al.addAll(list);
			}

			return al.iterator();
		} else {
			return Collections.EMPTY_LIST.iterator();
		}
	}

	public List getChild(String childName) {
		return (List) children.get(childName);
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return Returns the value.
	 */
	public String getValue() {
		return value;
	}

	public boolean hasChildren() {
		return (children != null) && (children.size() > 0);
	}

	public XMLStreamWriter outputXML(XMLStreamWriter parser, String prefix,
			String namespace) throws ACDCException {
		try {
			if ((prefix != null) && (namespace != null)) {
				parser.writeStartElement(prefix, name, namespace);
			} else {
				parser.writeStartElement(name);
			}
			
			if (attributes != null) {
				List attributeList = new ArrayList(attributes.keySet());
				for (int i = 0; i < attributeList.size(); i++) {
					String an = (String) attributeList.get(i);
					String av = prefix + ":" + (String) attributes.get(an);
					parser.writeAttribute(ACDCRequestHandler.XML_SCHEMA_NS, an, av);
				}
			}

			if (hasChildren()) {
				final Iterator i = children.values().iterator();
				while (i.hasNext()) {
					final Iterator childListIter = ((List) i.next()).iterator();
					while (childListIter.hasNext()) {
						((XMLElement) childListIter.next()).outputXML(parser,
								prefix, namespace);
					}
				}
			} else {
				if ((value != null) && (value.length() > 0)) {
					parser.writeCharacters(value);
				}
			}

			parser.writeEndElement();
		} catch (final XMLStreamException ex) {
            ex.printStackTrace();
        	StackTraceElement[] eTrace = new Throwable().fillInStackTrace().getStackTrace();
            log.warn("Failed - " + eTrace[1].getClassName()
    				+ "-" +  eTrace[1].getMethodName() 
    				+ " caught Exception - " 
    				+ ex.getClass().getName() + ": " + ex.getMessage());
			throw new ACDCException("Failed", ex);
		}

		return parser;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param value
	 *            The value to set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	Map customFields = null;

	public void substitute(Payload requestFields, Map pm_mCustomFields) {
		customFields = pm_mCustomFields;
		if (hasChildren()) {
			final Iterator i = children.values().iterator();
			while (i.hasNext()) {
				final Iterator childListIter = ((List) i.next()).iterator();
				while (childListIter.hasNext()) {
					((XMLElement) childListIter.next()).substitute(
							requestFields, pm_mCustomFields);
				}
			}
		} else if (isSubstitution()) {
			final String fullSpecifiedValue = stripBrackets(value);
			debugTrace("isSubstitution<" + fullSpecifiedValue + ">");
			if (fullSpecifiedValue.startsWith(REQUEST_PREFIX)) {
				final String requestStrippedName = stripRequestPrefix(fullSpecifiedValue);
				final List l = requestFields.getEntries(requestStrippedName);
				if ((l == null) || (l.size() == 0)) {
					log.warn("No value for Request substitution: "
							+ fullSpecifiedValue);
				}

				if (l.size() > 1) {
					log.warn("More than one possible value, using the first: "
							+ fullSpecifiedValue);
				}

				value = ((XMLElement) l.get(0)).getValue();
			} else if (isMath(SUB_PREFIX + fullSpecifiedValue)) {
				value = doMath(requestFields, fullSpecifiedValue);
				//Calculate Exchange rate from two amounts
			} else if (fullSpecifiedValue.startsWith(FX_PREFIX)) {
				final NumberFormat currencyFormatter = NumberFormat
						.getCurrencyInstance(Locale.ENGLISH);

				String firstValue = "0.00";
				String secondValue = "0.00";
				final String requestStripped = stripPrefix(fullSpecifiedValue);

				// Get the first value from the Request or String.
				if (requestStripped.startsWith(REQUEST_PREFIX)) {
					final String tempString = stripRequestPrefix(requestStripped);
					final String requestStrippedName = stripFromSeperator(tempString);
					final List l = requestFields
							.getEntries(requestStrippedName);
					if ((l == null) || (l.size() == 0)) {
						log.warn("No value for Request substitution: "
								+ fullSpecifiedValue);
					}

					if (l.size() > 1) {
						log.warn("More than one possible value, using the first: "
								+ fullSpecifiedValue);
					}
					firstValue = ((XMLElement) l.get(0)).getValue();
				} else {
					if (isValueFunction(requestStripped)) {
						firstValue = doValueFunction(requestFields,
								requestStripped);
					} else {
						firstValue = getValue(requestStripped);
					}
				}

				// Get the last value from the Request or String.
				final String fromSeperator = stripThroughSeperator(requestStripped);
				if (fromSeperator.trim().startsWith(REQUEST_PREFIX)) {
					final String tempString = stripRequestPrefix(fromSeperator);
					final String requestStrippedName = stripMathPostfix(tempString);
					final List l = requestFields
							.getEntries(requestStrippedName);
					if ((l == null) || (l.size() == 0)) {
						log.warn("No value for Request substitution: "
								+ fullSpecifiedValue);
					}

					if (l.size() > 1) {
						log.warn("More than one possible value, using the first: "
								+ fullSpecifiedValue);
					}
					secondValue = ((XMLElement) l.get(0)).getValue();
				} else {
					if (isValueFunction(requestStripped)) {
						secondValue = doValueFunction(requestFields,
								requestStripped);
					} else {
						secondValue = getValue(fromSeperator);
					}
				}
				final BigDecimal first = new BigDecimal(firstValue.trim());
				final BigDecimal second = new BigDecimal(secondValue.trim());
				BigDecimal total = new BigDecimal("0.00");
					final BigDecimal firstPlus = first.multiply(new BigDecimal(
							"1.000"));
					total = firstPlus.divide(second, BigDecimal.ROUND_CEILING);
					final String temp = total.toString().concat("000000");
					final int length = temp.length();
					final int location = temp.indexOf('.');
					if (length > location + 7) {
						value = temp.substring(0, location + 7);
					} else {
						value = temp;
					}
			}

			// FOR THREE INPUT VALUE FUNCTIONS
			else if (fullSpecifiedValue.startsWith(FX_DEDUCT_FEE_PREFIX)
					|| fullSpecifiedValue.startsWith(FX_MINUS_FEE_PREFIX)) {
				final NumberFormat currencyFormatter = NumberFormat
						.getCurrencyInstance(Locale.ENGLISH);

				String firstValue = "0.00";
				String secondValue = "0.00";
				String thirdValue = "0.00";
				final String requestStripped = stripPrefix(fullSpecifiedValue);

				// Get the first value from the Request or String.
				if (requestStripped.startsWith(REQUEST_PREFIX)) {
					final String tempString = stripRequestPrefix(requestStripped);
					final String requestStrippedName = stripFromSeperator(tempString);
						final List l = requestFields
						.getEntries(requestStrippedName);
					if ((l == null) || (l.size() == 0)) {
						log.warn("No value for Request substitution: "
								+ fullSpecifiedValue);
					}
	
					if (l.size() > 1) {
						log.warn("More than one possible value, using the first: "
								+ fullSpecifiedValue);
					}
					firstValue = ((XMLElement) l.get(0)).getValue();
				} else {
					firstValue = getValue(requestStripped);
				}

				// Get the middle value from the Request or String.
				final String middleSeperator = stripThroughSeperator(requestStripped);
				if (middleSeperator.trim().startsWith(REQUEST_PREFIX)) {
					final String tempString = stripRequestPrefix(middleSeperator);
					final String requestStrippedName = stripMathPostfix(tempString);
					final List l = requestFields
							.getEntries(requestStrippedName);
					if ((l == null) || (l.size() == 0)) {
						log.warn("No value for Request substitution: "
								+ fullSpecifiedValue);
					}

					if (l.size() > 1) {
						log.warn("More than one possible value, using the first: "
								+ fullSpecifiedValue);
					}
					secondValue = ((XMLElement) l.get(0)).getValue();
				} else {
					secondValue = getValue(middleSeperator);
				}

				// Get the last value from the Request or String.
				final String fromSeperatorLast = stripThroughSeperator(middleSeperator);
				if (fromSeperatorLast.trim().startsWith(REQUEST_PREFIX)) {
					final String tempString = stripRequestPrefix(fromSeperatorLast);
					final String requestStrippedName = stripMathPostfix(tempString);
					final List l = requestFields
							.getEntries(requestStrippedName);
					if ((l == null) || (l.size() == 0)) {
						log.warn("No value for Request substitution: "
								+ fullSpecifiedValue);
					}

					if (l.size() > 1) {
						log.warn("More than one possible value, using the first: "
								+ fullSpecifiedValue);
					}
					thirdValue = ((XMLElement) l.get(0)).getValue();
				} else {
					thirdValue = getValue(fromSeperatorLast);
				}

				final BigDecimal first = new BigDecimal(firstValue.trim());
				final BigDecimal second = new BigDecimal(secondValue.trim());
				final BigDecimal third = new BigDecimal(thirdValue.trim());
				BigDecimal total = new BigDecimal("0.00");
				if (fullSpecifiedValue.startsWith(FX_MINUS_FEE_PREFIX)) { // FX_MINUS_FEE
					// firstValue - The amount you are requesting a quote on
					// secondValue - The fx rate for the conversion.
					// thirdValue - The fee amount

					total = (first.multiply(second).subtract(third));
					final String temp = total.toString();
					final int length = temp.length();
					final int location = temp.indexOf('.');
					if (length > location + 3) {
						value = temp.substring(0, location + 3);
					} else {
						value = temp;
					}

				} else { // FX_DEDUCT_FEE
					// firstValue - The amount Deduct fee amount you are
					// requesting a quote on
					// secondValue - The fx rate for the conversion.
					// thirdValue - The fee amount

					total = (first.subtract(third)).multiply(second);
					final String temp = total.toString();
					final int length = temp.length();
					final int location = temp.indexOf('.');
					if (length > location + 3) {
						value = temp.substring(0, location + 3);
					} else {
						value = temp;
					}

				}
			} else if (fullSpecifiedValue.startsWith(REV_QUOTE_SEND_TOTAL_PREFIX)) {
				value = doRevQuote(requestFields, fullSpecifiedValue);
			} else if (fullSpecifiedValue.startsWith(RECV_AMT_PREFIX)) {
				value = doRecvAmt(requestFields, fullSpecifiedValue);
			} else if (fullSpecifiedValue.equalsIgnoreCase(TIMESTAMP_SUB)) {
				value = getCurrentDateString();
			} else if (fullSpecifiedValue.equalsIgnoreCase(REFERENCE_NUMBER)) {
				// Rob Morgan
				final Random rng = new Random();
				value = Integer.toString(rng.nextInt(90000000) + 10000000);
			} else if (fullSpecifiedValue.startsWith(FX_RATE_PREFIX)) {
				value = doFxRate(requestFields, fullSpecifiedValue);
			} else {
				final Object valueObj = pm_mCustomFields.get(fullSpecifiedValue);
				if (valueObj == null) {
					log.warn("No value for Custom substitution: "
							+ fullSpecifiedValue);
				}
				value = valueObj.toString();
			}

		}
	}

	/**
	 * 
	 * @return
	 * @author
	 */
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("XMLElement[");
		buffer.append("children = ").append(children);
		buffer.append(" name = ").append(name);
		buffer.append(" value = ").append(value);
		buffer.append("]");
		return buffer.toString();
	}
	
	private String doFxRate(Payload pm_requestFields,
			String pm_sFullSpecifiedValue) {
		debugTrace("doFxRate<" + pm_sFullSpecifiedValue + ">");
		String sRetValue = "0.00";
		String firstValue = "USD";
		String secondValue = "USD";
		final String requestStripped = stripPrefix(pm_sFullSpecifiedValue);

		// Get the first value from the Request or String.
		if (requestStripped.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(requestStripped);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields
					.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			} else {
				if (l.size() > 1) {
					log.warn("More than one possible value, using the first: "
							+ pm_sFullSpecifiedValue);
				}
				firstValue = ((XMLElement) l.get(0)).getValue();
			}
		} else {
			firstValue = getValue(requestStripped);
		}

		// Get the last value from the Request or String.
		final String fromSeperator = stripThroughSeperator(requestStripped);
		if (fromSeperator.trim().startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(fromSeperator);
			final String requestStrippedName = stripMathPostfix(tempString);
			final List l = pm_requestFields
					.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			} else {
				if (l.size() > 1) {
					log.warn("More than one possible value, using the first: "
							+ pm_sFullSpecifiedValue);
				}
				secondValue = ((XMLElement) l.get(0)).getValue();
			}

		} else {
			secondValue = getValue(fromSeperator);
		}

		sRetValue = FxRates.GetFxRate(firstValue, secondValue);
		debugTrace("doFxRate<" + pm_sFullSpecifiedValue + "> = "+sRetValue);
		return sRetValue;
	}

	private String doRecvAmt(Payload pm_requestFields,
			String pm_sFullSpecifiedValue) {
		debugTrace("doRecvAmt<" + pm_sFullSpecifiedValue + ">");
		String sRetValue = "0.00";
		final NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(Locale.ENGLISH);

		String firstValue = "USD";
		String secondValue = "USD";
		String thirdValue = "0.00";
		String fourthValue = "0.00";
		final String requestStripped = stripPrefix(pm_sFullSpecifiedValue);

		// Get the first value from the Request or String.
		if (requestStripped.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(requestStripped);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			} else {
				if (l.size() > 1) {
					log.warn("More than one possible value, using the first: "
							+ pm_sFullSpecifiedValue);
				}
				firstValue = ((XMLElement) l.get(0)).getValue();
			}

		} else {
			firstValue = getValue(requestStripped);
		}
		debugTrace("firstValue<" + firstValue + ">");

		// Get the second value from the Request or String.
		final String secondSeperator = stripThroughSeperator(requestStripped);
		debugTrace("secondSeperator<" + secondSeperator + ">");
		if (secondSeperator.trim().startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(secondSeperator);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			secondValue = ((XMLElement) l.get(0)).getValue();
		} else {
			secondValue = getValue(secondSeperator);
		}
		debugTrace("secondValue<" + secondValue + ">");

		// Get the third value from the Request or String.
		String thirdSeperator = stripThroughSeperator(secondSeperator).trim();
		debugTrace("thirdSeperator<" + thirdSeperator + ">");
		if (thirdSeperator.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(thirdSeperator);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			thirdValue = ((XMLElement) l.get(0)).getValue();
		} else {
			if (isValueFunction(thirdSeperator)) {
				int iIdx = getFirstSubFunctionEndIdx(thirdSeperator);
//				thirdSeperator = stripPrefix(thirdSeperator.substring(0, iIdx));
//				thirdSeperator = thirdSeperator.substring(0, iIdx);
				thirdValue = doValueFunction(pm_requestFields,
						thirdSeperator);
				// Skip over math.
				thirdSeperator = thirdSeperator.substring(iIdx);
				debugTrace("requestPostMath<" + thirdSeperator + ">");
			} else {
				thirdValue = getValue(thirdSeperator);
			}
			
			
			
//			thirdValue = getValue(thirdSeperator);
		}

		debugTrace("thirdValue<" + thirdValue + ">");
		// Get the last value from the Request or String.
		String fromSeperatorLast = stripThroughSeperator(thirdSeperator).trim();
		debugTrace("fromSeperatorLast<" + fromSeperatorLast + ">");
		if (fromSeperatorLast.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(fromSeperatorLast);
			final String requestStrippedName = stripMathPostfix(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			fourthValue = ((XMLElement) l.get(0)).getValue();
		} else {
			if (isValueFunction(fromSeperatorLast)) {
				int iIdx = getFirstSubFunctionEndIdx(fromSeperatorLast);
//				fromSeperatorLast = stripPrefix(thirdSeperator.substring(0, iIdx));
				fourthValue = doValueFunction(pm_requestFields,
						fromSeperatorLast);
				// Skip over math.
				fromSeperatorLast = pm_sFullSpecifiedValue.substring(iIdx);
				debugTrace("requestPostMath<" + fromSeperatorLast + ">");
			} else {
				fourthValue = getValue(fromSeperatorLast);
			}
			
//			fourthValue = getValue(fromSeperatorLast);
		}

		debugTrace("fourthValue<" + fourthValue + ">");
		final BigDecimal third = new BigDecimal(thirdValue.trim());
		final BigDecimal fourth = new BigDecimal(fourthValue.trim());
		BigDecimal total = new BigDecimal("0.00");

		if (pm_sFullSpecifiedValue.startsWith(RECV_AMT_PREFIX)) {
			// firstValue - Send Currency
			// secondValue - Receive Currency.
			// thirdValue - Send amount
			// fourthValue - The fee amount

			final BigDecimal fxRate = new BigDecimal(FxRates.GetFxRate(firstValue,
					secondValue));
			total = third.multiply(fxRate).subtract(fourth);
			sRetValue = limitDecimalPlaces(total);
		}
		debugTrace("doRecvAmt<" + pm_sFullSpecifiedValue + "> = "+sRetValue);
		return sRetValue;
	}

	private String doRevQuote(Payload pm_requestFields,
			String pm_sFullSpecifiedValue) {
		debugTrace("doRevQuote<" + pm_sFullSpecifiedValue + ">");
		String sRetValue = "0.00";
		String firstValue = "0.00";
		String secondValue = "0.00";
		String thirdValue = "0.00";
		String requestStripped = stripPrefix(pm_sFullSpecifiedValue);
		
		if (requestStripped.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(requestStripped);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			} else {
				if (l.size() > 1) {
					log.warn("More than one possible value, using the first: "
							+ pm_sFullSpecifiedValue);
				}
				firstValue = ((XMLElement) l.get(0)).getValue();
			}

		} else {
			firstValue = getValue(requestStripped);
			if (isValueFunction(requestStripped)) {
				int iIdx = getFirstSubFunctionEndIdx(requestStripped);
				firstValue = doValueFunction(pm_requestFields,
						requestStripped);
				// Skip over math.
				requestStripped = requestStripped.substring(iIdx);
				debugTrace("requestPostMath<" + requestStripped + ">");
			} else {
				firstValue = getValue(requestStripped);
			}
		}
		
		debugTrace("firstValue<" + firstValue + ">");
		
		// Get the second value from the Request or String.
		String secondSeperator = stripThroughSeperator(requestStripped);
		debugTrace("secondSeperator<" + secondSeperator + ">");
		if (secondSeperator.trim().startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(secondSeperator);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			secondValue = ((XMLElement) l.get(0)).getValue();
		} else {
			if (isValueFunction(secondSeperator)) {
				int iIdx = getFirstSubFunctionEndIdx(secondSeperator);
				secondValue = doValueFunction(pm_requestFields,
						secondSeperator);
				// Skip over math.
				secondSeperator = secondSeperator.substring(iIdx);
				debugTrace("requestPostMath<" + secondSeperator + ">");
			} else {
				secondValue = getValue(secondSeperator);
			}
		}
		debugTrace("secondValue<" + secondValue + ">");
		
		
		// Get the third value from the Request or String.
		String thirdSeperator = stripThroughSeperator(secondSeperator).trim();
		debugTrace("thirdSeperator<" + thirdSeperator + ">");
		if (thirdSeperator.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(thirdSeperator);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			thirdValue = ((XMLElement) l.get(0)).getValue();
		} else {
			if (isValueFunction(thirdSeperator)) {
				int iIdx = getFirstSubFunctionEndIdx(thirdSeperator);
				thirdValue = doValueFunction(pm_requestFields,
						thirdSeperator);
				// Skip over math.
				thirdSeperator = thirdSeperator.substring(iIdx);
				debugTrace("requestPostMath<" + thirdSeperator + ">");
			} else {
				thirdValue = getValue(thirdSeperator);
			}
		}

		debugTrace("thirdValue<" + thirdValue + ">");
		
		final BigDecimal first = new BigDecimal(firstValue.trim());
		final BigDecimal second = new BigDecimal(secondValue.trim());
		final BigDecimal third = new BigDecimal(thirdValue.trim());
		BigDecimal total = new BigDecimal("0.00");
		
		// firstValue - The receive amount you are requesting a
		// quote on
		// secondValue - The fx rate for the conversion.
		// thirdValue - The fee amount

		total = (first.divide(second, BigDecimal.ROUND_CEILING))
				.add(third);
		final String temp = total.toString();
		final int length = temp.length();
		final int location = temp.indexOf('.');
		if (length > location + 3) {
			value = temp.substring(0, location + 3);
		} else {
			value = temp;
		}
		
		debugTrace("doRevQuote<" + pm_sFullSpecifiedValue + "> = "+sRetValue);
		return sRetValue;
	}
	
	private String doValueFunction(Payload pm_requestFields,
			String pm_sFullSpecifiedValue) {
		String sRetValue = "0.00";
		debugTrace("doValueFunction<" + pm_sFullSpecifiedValue + ">");
		if (isMath(pm_sFullSpecifiedValue)) {
			sRetValue = doMath(pm_requestFields, pm_sFullSpecifiedValue.substring(2));
		} else if (isRecvAmt(pm_sFullSpecifiedValue)) {
			sRetValue = doRecvAmt(pm_requestFields, pm_sFullSpecifiedValue.substring(2));
		} else if (isFxRate(pm_sFullSpecifiedValue)) {
			sRetValue = doFxRate(pm_requestFields, pm_sFullSpecifiedValue.substring(2));
		}
		return sRetValue;
	}

	private String doMath(Payload pm_requestFields, 
			String pm_sFullSpecifiedValue) {
		String sRetValue = "";
		String firstValue = "0.00";
		String secondValue = "0.00";
		
		debugTrace("doMath<" + pm_sFullSpecifiedValue + ">");
		
		int iIdx = getFirstSubFunctionEndIdx(pm_sFullSpecifiedValue);
		String requestStripped;
		/*
		 * If no subfunctions (iIdx == 0), then the request is the entire string
		 */
		if (iIdx == 0) {
			requestStripped = stripPrefix(pm_sFullSpecifiedValue.substring(0));
		} else {
			requestStripped = stripPrefix(pm_sFullSpecifiedValue.substring(0, iIdx));
		}
		
		String fromSeperator;
		debugTrace("requestStripped<" + requestStripped + ">");

		// Get the first value from the Request or String.
		if (requestStripped.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(requestStripped);
			final String requestStrippedName = stripFromSeperator(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			firstValue = ((XMLElement) l.get(0)).getValue();
		} else {
			if (isValueFunction(requestStripped)) {
				iIdx = getFirstSubFunctionEndIdx(pm_sFullSpecifiedValue);
				requestStripped = stripPrefix(pm_sFullSpecifiedValue.substring(0, iIdx));
				firstValue = doValueFunction(pm_requestFields,
							requestStripped);
				// Skip over math.
				requestStripped = pm_sFullSpecifiedValue.substring(iIdx);
				debugTrace("requestPostMath<" + requestStripped + ">");
			} else {
				firstValue = getValue(requestStripped);
			}
		}
		debugTrace("firstValue<" + firstValue + ">");

		// Get the next value from the Request or String.
		fromSeperator = stripThroughSeperator(requestStripped).trim();
		if (fromSeperator.startsWith(REQUEST_PREFIX)) {
			final String tempString = stripRequestPrefix(fromSeperator);
			final String requestStrippedName = stripMathPostfix(tempString);
			final List l = pm_requestFields.getEntries(requestStrippedName);
			if ((l == null) || (l.size() == 0)) {
				log.warn("No value for Request substitution: "
						+ pm_sFullSpecifiedValue);
			}

			if (l.size() > 1) {
				log.warn("More than one possible value, using the first: "
						+ pm_sFullSpecifiedValue);
			}
			secondValue = ((XMLElement) l.get(0)).getValue();
		} else {
			if (isValueFunction(fromSeperator)) {
				secondValue = doValueFunction(pm_requestFields,
						fromSeperator);
			} else {
				secondValue = getValue(fromSeperator);
			}
		}
		debugTrace("secondValue<" + secondValue + ">");
		final BigDecimal first = new BigDecimal(firstValue.trim());
		final BigDecimal second = new BigDecimal(secondValue.trim());
		BigDecimal total = new BigDecimal("0.00");
		if (pm_sFullSpecifiedValue.startsWith(ADDITION_PREFIX)) {
			total = first.add(second);
			sRetValue = total.toString();
		} else if (pm_sFullSpecifiedValue.startsWith(SUBTRACTION_PREFIX)) {
			total = first.subtract(second);
			sRetValue = total.toString();
		} else if (pm_sFullSpecifiedValue.startsWith(DIVISION_PREFIX)) {
			total = first.divide(second, BigDecimal.ROUND_CEILING);
			sRetValue = limitDecimalPlaces(total);
		} else if (pm_sFullSpecifiedValue.startsWith(MULTIPLICATION_PREFIX)) {
			total = first.multiply(second);
			sRetValue = limitDecimalPlaces(total);
		}
		debugTrace("doMath<" + pm_sFullSpecifiedValue + "> = "+sRetValue);
		return sRetValue;
	}

	private String getCurrentDateString() {
		final String almostFormat = ISO8601FORMAT.format(new Date());
		final int index = almostFormat.length() - 2;
		return almostFormat.substring(0, index) + ':'
				+ almostFormat.substring(index);
	}

	private boolean isFxRate(String pm_sfullSpecifiedValue) {
		boolean bRetValue = false;
		if (pm_sfullSpecifiedValue.startsWith(SUB_PREFIX + FX_RATE_PREFIX)) {
			bRetValue = true;
		}
		return bRetValue;
	}

	private boolean isMath(String pm_sfullSpecifiedValue) {
		boolean bRetValue = false;
		if (pm_sfullSpecifiedValue.startsWith(SUB_PREFIX + ADDITION_PREFIX)
				|| pm_sfullSpecifiedValue.startsWith(SUB_PREFIX
						+ SUBTRACTION_PREFIX)
				|| pm_sfullSpecifiedValue.startsWith(SUB_PREFIX
						+ MULTIPLICATION_PREFIX)
				|| pm_sfullSpecifiedValue.startsWith(SUB_PREFIX
						+ DIVISION_PREFIX)) {
			bRetValue = true;
		}
		return bRetValue;
	}

	private boolean isRecvAmt(String pm_sfullSpecifiedValue) {
		boolean bRetValue = false;
		if (pm_sfullSpecifiedValue.startsWith(SUB_PREFIX + RECV_AMT_PREFIX)) {
			bRetValue = true;
		}
		return bRetValue;
	}

	private boolean isValueFunction(String pm_sfullSpecifiedValue) {
		boolean bRetValue = false;
		if (isMath(pm_sfullSpecifiedValue)
				|| isFxRate(pm_sfullSpecifiedValue)
				|| isRecvAmt(pm_sfullSpecifiedValue)) {
			bRetValue = true;
		}
		return bRetValue;
	}

	private boolean isSubstitution() {
		if (value == null) {
			return false;
		}

		String valStr = value.toString();
		if (valStr == null) {
			return false;
		}
		valStr = valStr.trim();
		if (valStr.length() == 0) {
			return false;
		}

		return valStr.startsWith(SUB_PREFIX) && valStr.endsWith(SUB_POSTFIX);
	}

	/**
	 * @param string
	 * @return
	 */
	private String stripBrackets(String string) {
		return string.substring(2, string.length() - 1);
	}

	private String stripFromSeperator(String value) {
		return value.substring(0, value.indexOf(","));
	}

	private String stripMathPostfix(String value) {
		return value.substring(0, value.indexOf("]"));
	}

	private String stripEveryThing(String value) {
		String sTemp = value;
		int iTemp;
		
		iTemp = sTemp.indexOf(",");
		if (iTemp > -1) {
			sTemp = stripFromSeperator(sTemp);
		}
		
		iTemp = sTemp.indexOf("]");
		if (iTemp > -1) {
			sTemp = stripMathPostfix(sTemp);
		}
		
		return sTemp;
	}

	private String stripPrefix(String pm_sValue) {
		String retValue;
		if (pm_sValue.startsWith(SUBTRACTION_PREFIX)) {
			retValue = pm_sValue.substring(SUBTRACTION_PREFIX.length());
		} else if (pm_sValue.startsWith(MULTIPLICATION_PREFIX)) {
			retValue = pm_sValue.substring(MULTIPLICATION_PREFIX.length());
		} else if (pm_sValue.startsWith(DIVISION_PREFIX)) {
			retValue = pm_sValue.substring(DIVISION_PREFIX.length());
		} else if (pm_sValue.startsWith(FX_PREFIX)) {
			retValue = pm_sValue.substring(FX_PREFIX.length());
		} else if (pm_sValue.startsWith(FX_RATE_PREFIX)) {
			retValue = pm_sValue.substring(FX_RATE_PREFIX.length());
		} else if (pm_sValue.startsWith(REV_QUOTE_SEND_TOTAL_PREFIX)) {
			retValue = pm_sValue.substring(REV_QUOTE_SEND_TOTAL_PREFIX.length());
		} else if (pm_sValue.startsWith(FX_DEDUCT_FEE_PREFIX)) {
			retValue = pm_sValue.substring(FX_DEDUCT_FEE_PREFIX.length());
		} else if (pm_sValue.startsWith(FX_MINUS_FEE_PREFIX)) {
			retValue = pm_sValue.substring(FX_MINUS_FEE_PREFIX.length());
		} else if (pm_sValue.startsWith(RECV_AMT_PREFIX)) {
			retValue = pm_sValue.substring(RECV_AMT_PREFIX.length());
		} else {
			retValue = pm_sValue.substring(ADDITION_PREFIX.length());
		}

		return retValue;
	}

	private String stripRequestPrefix(String value) {
		return value.substring(REQUEST_PREFIX.length());
	}

	private String stripSubChars(String value) {
		return value.substring(2, value.length() - 1);
	}

	private String stripThroughMathPostfixSeperator(String value) {
		return value.substring(value.indexOf("]") + 1);
	}

	private String stripThroughSeperator(String value) {
		return value.substring(value.indexOf(",") + 1);
	}
	
	private String limitDecimalPlaces(BigDecimal pm_bdValue) {
		String sRetValue;
		final String temp = pm_bdValue.toString();
		final int length = temp.length();
		final int location = temp.indexOf('.');
		if (length > location + 3) {
			sRetValue = temp.substring(0, location + 3);
		} else {
			sRetValue = temp;
		}
		return sRetValue;
	}
	
	private String getValue(String requestStripped) {
		String sRetValue="";
		String sTemp = customValue(stripEveryThing(requestStripped));
		if (sTemp != null) {
			sRetValue = sTemp;
		} else {
			sRetValue = stripEveryThing(requestStripped);
		}
		return sRetValue;
	}
	
	private String customValue(String fullSpecifiedValue) {
		String sRetValue = null;
		if (customFields != null) {
			int iEnd = fullSpecifiedValue.indexOf('}');
			if (fullSpecifiedValue.startsWith(SUB_PREFIX) && (iEnd > 1)) {
				String sName = fullSpecifiedValue.substring(2, iEnd); 
				final Object valueObj = customFields.get(sName);
				if (valueObj != null) {
					sRetValue = valueObj.toString();
				}
			}
		}
		return sRetValue;
	}
	
	private int getFirstSubFunctionEndIdx(String pm_sCmdLine) {
		debugTrace("getFunctionEndIdx<" + pm_sCmdLine + ">");
		int iRetValue = 0;
		int iLen = pm_sCmdLine.length();
		
		char cStartChar = '[';
		char cEndChar = ']';
		int iCnt = 0;
		if (pm_sCmdLine.startsWith(SUB_PREFIX)) {
			cStartChar = '{';
			cEndChar = '}';
			iCnt = 1;
		}
		
		int iFirstBracket = pm_sCmdLine.indexOf(cStartChar);
		char cTestChar = 0;
		for (int ii = iFirstBracket+1; ii < iLen; ii++) {
			cTestChar = pm_sCmdLine.charAt(ii); 
			if ( cTestChar == cStartChar) {
				iCnt++;
			} else if (cTestChar == cEndChar) {
				iCnt--;
				if (iCnt <= 0) {
					iRetValue = ii+1;
					break;
				}
			}
		}
		debugTrace("FirstHalf<" + pm_sCmdLine.substring(0, iRetValue) + ">");
		debugTrace("SecondHalf<" + pm_sCmdLine.substring(iRetValue) + ">");
		return iRetValue;
	}
	
	/*
	 * Common place for debug traces, so can be commented on/off in one place.
	 */
	public void debugTrace (String pm_sText) {
		if (CompileTimeFlags.debug) {
			log.debug(pm_sText);
		}
	}

}
