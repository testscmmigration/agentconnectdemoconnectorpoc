/*
 * Created on Jun 21, 2006
 *
 */
package com.moneygram.acdc;

import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author T007
 *
 */
public class Template {
    private static final Logger log = Logger.getLogger(Template.class);
    
    public static final String SUB_PREFIX = "${";
    public static final String SUB_POSTFIX= "}";
    public static final String REQUEST_PREFIX = "Request.";
    public static final String TIMESTAMP_SUB = "timestamp";
    public static final String ADDITION_PREFIX ="ADD[";
    public static final String SUBTRACTION_PREFIX ="SUB[";
    public static final String MATH_POSTFIX="]";
    public static final String MULTIPLICATION_PREFIX ="MULT[";
    public static final String DIVISION_PREFIX ="DIV[";
    
    private String templateName;
    private Template parent;
    private XMLElement root;
    
    public Template(String templateName) {
        super();
        this.templateName = templateName;
        root = new XMLElement(templateName);
    }
    
    public Template(String templateName, Template t){
        this(templateName);
        parent=t;
    }
    
    
    public XMLElement substitute(Payload requestFields, Map customFields) {
        XMLElement myClonedRoot = (XMLElement)root.clone();
        myClonedRoot.substitute(requestFields, customFields);

        if (parent != null) {
            XMLElement mapToBuild = parent.substitute(requestFields, customFields);
            mapToBuild.combineChildren(myClonedRoot);
            return mapToBuild;
        }
        else
            return myClonedRoot;
    }
    
    
    /**
     * @return Returns the templateName.
     */
    public String getTemplateName() {
        return templateName;
    }
    /**
     * @param templateName The templateName to set.
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
    
    
    
    /**
     * @return Returns the root.
     */
    protected XMLElement getRoot() {
        return root;
    }
    /**
     * @param root The root to set.
     */
    protected void setRoot(XMLElement root) {
        this.root = root;
    }
    /**
     * 
     * @return 
     * @author 
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Template[");
        buffer.append(" parent = ").append(parent);
        buffer.append(" templateName = ").append(templateName);
        buffer.append("]");
        return buffer.toString();
    }}
