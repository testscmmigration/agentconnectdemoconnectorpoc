/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc.exp;

import com.moneygram.common.enum.Enum;

/**
 * @author T007
 *
 */
public class Operation extends Enum{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Operation EQUALS = new Operation("Equals");
    public static final Operation LESS_THAN = new Operation("LessThan");
    public static final Operation GREATER_THAN = new Operation("GreaterThan");
    public static final Operation EXISTS = new Operation("Exists");
    public static final Operation DOES_NOT_EXISTS = new Operation("DoesNotExists");
    public static final Operation NOT_EQUALS = new Operation("NotEquals");
    public static final Operation NUMBER_LESS_THAN = new Operation("NumberLessThan");
    public static final Operation NUMBER_GREATER_THAN = new Operation("NumberGreaterThan");
   

    /**
     * @param arg0
     */
    public Operation(String arg0) {
        super(arg0, true);
    }

}
