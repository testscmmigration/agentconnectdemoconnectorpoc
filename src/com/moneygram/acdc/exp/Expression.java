/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc.exp;

import com.moneygram.acdc.Payload;

/**
 * @author T007
 *
 */
public interface Expression {
    public boolean evaluate(Payload p);
}
