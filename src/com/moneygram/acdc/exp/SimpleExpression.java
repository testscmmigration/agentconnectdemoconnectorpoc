/*
 * Created on Jun 26, 2006
 *
 */
package com.moneygram.acdc.exp;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.moneygram.acdc.Payload;
import com.moneygram.acdc.XMLElement;
import com.moneygram.common.enum.EnumRegistry;

/**
 * @author T007
 *
 */
public class SimpleExpression implements Expression {
    private static final Logger log  = Logger.getLogger(SimpleExpression.class);
    
    private boolean caseInsensitive = false;
    private String field;
    private String value;
    private Operation operation;

    public SimpleExpression() {
    }

    public SimpleExpression(String field, String value, String operation) {
        setField(field);
        setValue(value);
        setOperation((Operation)EnumRegistry.getByName(Operation.class, operation));
    }

    public SimpleExpression(String field, String value, Operation operation) {
        setField(field);
        setValue(value);
        setOperation(operation);
    }
    
    public SimpleExpression(String field, String value, Operation operation, boolean caseInsensitive) {
        setField(field);
        setValue(value);
        setOperation(operation);
        setCaseInsensitive(caseInsensitive);
    }
    
	/**
	 * @return Returns the caseInsensitive.
	 */
	public boolean isCaseInsensitive() {
		return caseInsensitive;
	}
	/**
	 * @param caseInsensitive The caseInsensitive to set.
	 */
	public void setCaseInsensitive(boolean caseInsensitive) {
		this.caseInsensitive = caseInsensitive;
	}
    /**
     * @return Returns the field.
     */
    public String getField() {
        return field;
    }
    /**
     * @param field The field to set.
     */
    public void setField(String field) {
        this.field = field;
    }
    /**
     * @return Returns the value.
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value The value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    
    /**
     * @return Returns the operation.
     */
    public Operation getOperation() {
        return operation;
    }
    /**
     * @param operation The operation to set.
     */
    public void setOperation(Operation operation) {
        this.operation = operation;
    }
    
    /**
     * @see com.moneygram.acdc.exp.Expression#evaluate(com.moneygram.acdc.Payload)
     */
    public boolean evaluate(Payload p) {
        List l = p.getEntries(getField());
        if (l == null || l.size() ==0) {
        	Operation o = getOperation();
        	
        	if (o == Operation.EXISTS) {
        		return false;
        	}
        	else if ( o == Operation.DOES_NOT_EXISTS) {
        		return true;
        	}
        	else {
                log.warn("Could not find specified property:" 
                		+ p.getApiName()+ ":"
                		+ field + ":"
                		+ o.getName() + ":"
                		+ value + ":"
                		);
                return false;
        	}
        }
        
        Iterator i = l.iterator();
        
        while(i.hasNext()) {
           String prop = ((XMLElement)i.next()).getValue();
           if (prop != null) {
           	   Operation o = getOperation();
           	   if (o == Operation.EQUALS) {
	               String propStr = prop.toString();
	               if (isCaseInsensitive()) {
		               if (propStr.equalsIgnoreCase(getValue()))
		                   return true;
	               }
	               else {
		               if (propStr.equals(getValue()))
		                   return true;
	               }
           	   }
           	   else if (o == Operation.EXISTS) {
           		   if (prop.length() > 0)
         	    	  return true;
         	   }
           	   else if (o == Operation.DOES_NOT_EXISTS) {
                   return false;
         	   }
           	   else if (o == Operation.NOT_EQUALS) {
 	               String propStr = prop.toString();
 	               if (isCaseInsensitive()) {
 		               if (!propStr.equalsIgnoreCase(getValue()))
 		                   return true;
 	               }
 	               else {
 		               if (!propStr.equals(getValue()))
 		                   return true;
 	               }
               }  
           	   else if (o == Operation.GREATER_THAN) {
	               String propStr = prop.toString();
		           if (propStr.compareTo(getValue()) < 0 ){
		           		return true;
		           }
        	   }
        	   else if (o == Operation.LESS_THAN) {
	               String propStr = prop.toString();
		           if (propStr.compareTo(getValue()) > 0 ){
		           		return true;
		           }
        	   }  
           	   else if (o == Operation.NUMBER_GREATER_THAN) {
           	   	   BigDecimal propBigDecimal = new BigDecimal(prop.toString());
           	   	   BigDecimal getDecimal = new BigDecimal(getValue());
		           if (propBigDecimal.compareTo(getDecimal) > 0 ){
		           		return true;
		           }
        	   }
        	   else if (o == Operation.NUMBER_LESS_THAN) {
        	   	   BigDecimal propBigDecimal = new BigDecimal(prop.toString());
           	   	   BigDecimal getDecimal = new BigDecimal(getValue());
		           if (propBigDecimal.compareTo(getDecimal) < 0 ){
		           		return true;
		           }
       	   }  

           }           
        }
        return false;
    }
}
